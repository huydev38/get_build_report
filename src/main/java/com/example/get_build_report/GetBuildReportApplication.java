package com.example.get_build_report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetBuildReportApplication {

    public static void main(String[] args) {
        SpringApplication.run(GetBuildReportApplication.class, args);
    }

}
