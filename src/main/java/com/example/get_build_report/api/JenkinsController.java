package com.example.get_build_report.api;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Base64;


@RestController
@RequestMapping("/api/pipeline")
public class JenkinsController {

    @Value("${jenkins.url}")
    private String jenkinsUrl;

    @Value("${jenkins.jobName}")
    private String jobName;

    @Value("${jenkins.username}")
    private String username;

    @Value("${jenkins.password}")
    private String password;

    @Value("${sonarqube.url}")
    private String sonarqubeUrl;

    @Value("${sonarqube.username}")
    private String sonarqubeUsername;

    @Value("${sonarqube.password}")
    private String sonarqubePassword;

    private final OkHttpClient client = new OkHttpClient();

    private String getAuthHeader() {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
        return "Basic " + new String(encodedAuth);
    }

    private String getSonarqubeAuthHeader() {
        String auth = sonarqubeUsername + ":" + sonarqubePassword;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes());
        return "Basic " + new String(encodedAuth);
    }

    @PostMapping("/run")
    public ResponseEntity<String> runPipeline() {
        String getCrumbsUrl = jenkinsUrl + "/crumbIssuer/api/json";
        Request crumbRequest = new Request.Builder()
                .url(getCrumbsUrl)
                .addHeader("Authorization", getAuthHeader())
                .build();

        try (Response crumbResponse = client.newCall(crumbRequest).execute()) {
            if (!crumbResponse.isSuccessful()) {
                throw new IOException("Failed to get crumb: " + crumbResponse.code() + " " + crumbResponse.message());
            }

            String crumbResponseString = crumbResponse.body().string();
            String crumb = crumbResponseString.split("\"crumb\":\"")[1].split("\"")[0];
            System.out.println("Crumb: " + crumb);

            String url = jenkinsUrl + "/job/" + jobName + "/build";
            Request request = new Request.Builder()
                    .url(url)
                    .post(RequestBody.create("", MediaType.parse("text/plain")))
                    .addHeader("Authorization", getAuthHeader())
                    .addHeader("Jenkins-Crumb", crumb)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                    throw new IOException("Failed to trigger build: " + response.code() + " " + response.message());
                }
                return ResponseEntity.ok("Pipeline triggered successfully.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error: " + e.getMessage());
        }
    }


    @GetMapping("/status")
    public ResponseEntity<String> getPipelineStatus() {
        String url = jenkinsUrl + "/job/" + jobName + "/lastBuild/api/json";
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", getAuthHeader())
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }

            String responseBody = response.body().string();
            return ResponseEntity.ok(responseBody);
        } catch (IOException e) {
            System.err.println("Error fetching pipeline status: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/report")
    public ResponseEntity<String> getPipelineReport(@RequestParam("branch") String branch) {
        String url = String.format("%s/job/%s/lastBuild/artifact/%s/trivy_report_%s.json", jenkinsUrl, jobName, branch, branch);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", getAuthHeader())
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }

            String responseBody = response.body().string();
            return ResponseEntity.ok(responseBody);
        } catch (IOException e) {
            System.err.println("Error fetching pipeline report: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @GetMapping("/report/sonarqube")
    public ResponseEntity<String> getSonarReport(@RequestParam("branch") String branch) {
        String url = String.format("%s/api/measures/component?component=Mini-Project-Backend-%s&metricKeys=complexity,duplicated_lines_density,violations,blocker_violations,critical_violations,major_violations,minor_violations,info_violations,code_smells,sqale_rating,bugs,reliability_rating,vulnerabilities,security_rating", sonarqubeUrl, branch);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", getSonarqubeAuthHeader())
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }

            String responseBody = response.body().string();
            return ResponseEntity.ok(responseBody);
        } catch (IOException e) {
            System.err.println("Error fetching SonarQube report: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

}
