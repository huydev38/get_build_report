pipeline {
    agent any

    tools {
        maven 'my-maven'
        gradle 'my-gradle'
    }

    environment {
        TELEGRAM_BOT_TOKEN = credentials('bot-telegram-token')
        TELEGRAM_CHAT_ID = credentials('bot-telegram-chatid')
        IMAGE_NAME = 'backend_app'
        DOCKERHUB = credentials('DockerHub')
    }

    stages {

        stage('Preparation') {
            steps {
                script {
                    sh 'echo "Preparing the environment"'
                    branches = ['develop', 'product', 'qa']
                }
            }
        }

        stage('SonarQube Analysis') {
            steps {
                script {
                    branches.each { branch ->
                        stage("Checkout and Scan ${branch}") {
                            dir("${branch}") {
                                checkout([$class: 'GitSCM',
                                    branches: [[name: branch]],
                                    userRemoteConfigs: [[url: 'https://gitlab.com/huydev38/mini_project_backend.git']]
                                ])

                                withSonarQubeEnv('SonarQube Scanner') {
                                    sh "mvn clean verify sonar:sonar -Dmaven.test.skip=true -Dsonar.projectKey=Mini-Project-Backend-${branch} -Dsonar.projectName='Mini Project Backend (${branch})'"
                                }
                                sleep(10)
                                qualitygate = waitForQualityGate()
                            }
                        }
                    }
                }
            }
        }

        stage('Pull Docker Image and Scan with Trivy') {
            steps {
                script {
                    branches.each { branch ->
                        stage("Scan with Trivy ${branch}") {
                            dir("${branch}") {
                                def environment = (branch == 'develop') ? 'dev' : (branch == 'product') ? 'prod' : branch
                                withDockerRegistry(credentialsId: 'DockerHub') {
                                    // Pull Docker Image
                                    sh "docker pull ${DOCKERHUB_USR}/${IMAGE_NAME}:${environment}"
                                    sh "trivy image --format json --output trivy_report_${branch}.json ${DOCKERHUB_USR}/${IMAGE_NAME}:${environment}"
                                }
                            }
                        }
                    }
                }
            }
        }

        stage('Archive Trivy Reports') {
            steps {
                script {
                    branches.each { branch ->
                        archiveArtifacts artifacts: "${branch}/trivy_report_${branch}.json", allowEmptyArchive: false
                    }
                }
            }
        }

        stage('Check for Code Changes in Main') {
            steps {
                script {
                    dir("main") {
                        checkout([$class: 'GitSCM',
                            branches: [[name: 'main']],
                            userRemoteConfigs: [[url: 'https://gitlab.com/huydev38/get_build_report.git']]
                        ])
                        def changes = sh(script: 'git diff HEAD~1 --name-only', returnStdout: true).trim()
                        if (changes) {
                            echo "Changes detected in main"
                            stash includes: '**/*', name: "source-main"
                            env.CODE_CHANGED = 'true'
                        } else {
                            echo "No changes detected in main"
                            env.CODE_CHANGED = 'false'
                        }
                    }
                }
            }
        }

        stage('Build and Deploy') {
            when {
                expression { return env.CODE_CHANGED == 'true' }
            }
            steps {
                script {
                    unstash 'source-main'
                    sh "gradle clean build -x test"
                    sh "docker build -t build_report_backend ."
                    sh "docker stop report_backend_container || true"
                    sh "docker rm report_backend_container || true"
                    sh "docker run -d --restart=always -p 8082:8080 --name report_backend_container build_report_backend"
                }
            }
        }
    }

    post {
        always {
            script {
                def message = "Jenkins Job '${env.JOB_NAME}' (${env.BUILD_NUMBER}) - ${currentBuild.currentResult} \n"
                sh """
                    curl -s -X POST https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage -d chat_id=${TELEGRAM_CHAT_ID} -d text="${message}"
                """
            }
            cleanWs()
        }
    }
}
