# Backend để lấy report code build từ Jenkins

### Các bước để chạy project

1. Clone project về máy
2. Tạo application.properties trong src/main/resources
3. Copy nội dung sau vào application.properties và thêm thông tin tương ứng
```spring.application.name=get_build_report
server.port=
jenkins.url=
jenkins.jobName=
jenkins.username=
jenkins.password=
sonarqube.url=
sonarqube.username=
sonarqube.password=
```