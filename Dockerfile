FROM openjdk:17-jdk-alpine

WORKDIR /app

COPY build/libs/get_build_report-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]